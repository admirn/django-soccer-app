# Simple Django Soccer game app

This is a simple Soccergame app. You can add 7 players for only your team.

## Installing (works with python2.7)
```
git clone https://admirn@bitbucket.org/admirn/django-soccer-app.git
```
```
cd django-soccer-app
```
```
virtualenv env
```

```
source ./env/bin/activate
```

```
pip install -r requirements.txt
```
```
cd soccerapp
```

```
python manage.py makemigrations
```

```
python manage.py migrate
```

```
python manage.py runserver
```
