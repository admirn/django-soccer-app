from django.db import models
from django.contrib.auth.models import User

class Game(models.Model):
    name = models.CharField(max_length=160)
    datetime = models.DateField()
    creator = models.ForeignKey(User, null=False, related_name='creator')
    goalkeeper = models.ForeignKey(User, blank=True, null=True, related_name='goalkeeper')
    defense2 = models.ForeignKey(User, blank=True, null=True, related_name='defense2')
    defense3 = models.ForeignKey(User, blank=True, null=True, related_name='defense3')
    middle4 = models.ForeignKey(User, blank=True, null=True, related_name='middle4')
    forward5 = models.ForeignKey(User, blank=True, null=True, related_name='forward5')
    forward6 = models.ForeignKey(User, blank=True, null=True, related_name='forward6')

    def __unicode__(self):
        return self.name
