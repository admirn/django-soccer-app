# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('soccergame', '0003_auto_20170530_1804'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='game',
            name='defense4',
        ),
        migrations.RemoveField(
            model_name='game',
            name='defense5',
        ),
        migrations.RemoveField(
            model_name='game',
            name='forward10',
        ),
        migrations.RemoveField(
            model_name='game',
            name='forward11',
        ),
        migrations.RemoveField(
            model_name='game',
            name='forward9',
        ),
        migrations.RemoveField(
            model_name='game',
            name='middle6',
        ),
        migrations.RemoveField(
            model_name='game',
            name='middle7',
        ),
        migrations.RemoveField(
            model_name='game',
            name='middle8',
        ),
        migrations.AddField(
            model_name='game',
            name='forward5',
            field=models.ForeignKey(related_name='forward5', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='forward6',
            field=models.ForeignKey(related_name='forward6', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='middle4',
            field=models.ForeignKey(related_name='middle4', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
