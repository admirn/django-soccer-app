# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('soccergame', '0002_game_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='defense2',
            field=models.ForeignKey(related_name='defense2', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='defense3',
            field=models.ForeignKey(related_name='defense3', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='defense4',
            field=models.ForeignKey(related_name='defense4', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='defense5',
            field=models.ForeignKey(related_name='defense5', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='forward10',
            field=models.ForeignKey(related_name='forward10', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='forward11',
            field=models.ForeignKey(related_name='forward11', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='forward9',
            field=models.ForeignKey(related_name='forward9', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='middle6',
            field=models.ForeignKey(related_name='middle6', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='middle7',
            field=models.ForeignKey(related_name='middle7', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='game',
            name='middle8',
            field=models.ForeignKey(related_name='middle8', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
