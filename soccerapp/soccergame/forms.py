from django import forms
import datetime
import account.forms


class SignupForm(account.forms.SignupForm):

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        del self.fields["username"]


class CreateGameForm(forms.Form):
    name = forms.CharField(label='Your name', max_length=160)
    date = forms.DateField(initial=datetime.date.today)

    # def clean_name(self):
    #     name = self.cleaned_data.get('name')
    #     if "admir" not in name:
    #         raise forms.ValidationError('Name must be admir.')
    #     return name
