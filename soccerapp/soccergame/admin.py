from django.contrib import admin
from soccergame.models import Game

class TaskGame(admin.ModelAdmin):
    list_display = ('id', 'datetime', 'creator',)
    list_display_links = ('id', 'creator',)
    search_fields = ('datetime', 'id')
    list_filter = ('datetime', 'creator',)
    list_per_page = 10

admin.site.register(Game, TaskGame)
