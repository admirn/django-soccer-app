from django.conf.urls import url
from . import views

urlpatterns = (
  url(r'^$', views.Index.as_view(), name='index'),
  url(r'^create_game/$', views.CreateGame.as_view(), name='create_game'),
  url(r'^list_of_my_games/$', views.ListMyGames.as_view(), name='list_of_my_games'),
  url(r'^list_of_all_games/$', views.ListGames.as_view(), name='list_of_all_games'),
  url(r'^game/(?P<pk>\d+)$', views.Game.as_view(), name='game'),
  url(r'^game/edit_goalkeeper/(?P<pk>\d+)$', views.edit_goalkeeper, name='edit_goalkeeper'),
  url(r'^game/edit_defence2/(?P<pk>\d+)$', views.edit_defence2, name='edit_defense2'),
  url(r'^game/edit_defence3/(?P<pk>\d+)$', views.edit_defence3, name='edit_defense3'),
  url(r'^game/edit_middle4/(?P<pk>\d+)$', views.edit_middle4, name='edit_middle4'),
  url(r'^game/edit_forward5/(?P<pk>\d+)$', views.edit_forward5, name='edit_forward5'),
  url(r'^game/edit_forward6/(?P<pk>\d+)$', views.edit_forward6, name='edit_forward6'),
  url(r'^game/delete/(?P<pk>\d+)$', views.game_delete, name='game_delete'),
)
