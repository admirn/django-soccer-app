from django.shortcuts import render

import account.forms
import account.views

from django.views.generic import TemplateView
from .forms import CreateGameForm
from .models import Game
from .models import Game as GameModel
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from django.views.generic.detail import DetailView

from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

class LoginView(account.views.LoginView):

    form_class = account.forms.LoginEmailForm

class Index(TemplateView):
    template_name = 'game/index.html'

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        content = {
            'test': 'testTekxt',
        }
        return render(request, self.template_name, content)

class CreateGame(TemplateView):
    template_name = 'game/create_game.html'
    form = CreateGameForm

    def get(self, request, *args, **kwargs):
        content = {
            'form': self.form,
        }
        return render(request, self.template_name, content)

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        model = GameModel
        if form.is_valid():
            GameModel.objects.create(
                name=form.cleaned_data['name'],
                datetime=form.cleaned_data['date'],
                creator=request.user,
            )
            messages.add_message(request, messages.INFO, 'Your comment was added.')
            messages.add_message(request, messages.SUCCESS, 'Add player for this game on list of games.')
            return HttpResponseRedirect(reverse('create_game'))
        form = form
        content = {
            'form': form,
        }
        return render(request, self.template_name, content)

class ListGames(TemplateView):
    template_name = 'game/list_of_all_games.html'
    model = GameModel

    def get_context_data(self, **kwargs):
       context = super(ListGames, self).get_context_data(**kwargs)
       context['games'] = GameModel.objects.all().order_by("-id")[:25]
       return context

class ListMyGames(TemplateView):
    template_name = 'game/list_of_my_games.html'
    model = GameModel

    def get_context_data(self, **kwargs):
       context = super(ListMyGames, self).get_context_data(**kwargs)
       context['games'] = GameModel.objects.all().filter(creator=self.request.user).order_by("-id")[:25]
       return context

class Game(DetailView):
    template_name = 'game/game.html'
    model = Game

    def get_context_data(self, **kwargs):
        context = super(Game, self).get_context_data(**kwargs)
        return context

def edit_goalkeeper(request, pk):
    template_name = 'game/edit_goalkeeper.html'

    if request.method == 'POST':
        selected_option = request.POST['goalkeeper']
        obj = GameModel.objects.get(id=pk)
        obj.goalkeeper = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Goalkeeper was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def edit_defence2(request, pk):
    template_name = 'game/edit_defense2.html'

    if request.method == 'POST':
        selected_option = request.POST['defense2']
        obj = GameModel.objects.get(id=pk)
        obj.defense2 = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Defense2 player was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def edit_defence3(request, pk):
    template_name = 'game/edit_defense3.html'

    if request.method == 'POST':
        selected_option = request.POST['defense3']
        obj = GameModel.objects.get(id=pk)
        obj.defense3 = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Defense3 player was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)

        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def edit_middle4(request, pk):
    template_name = 'game/edit_middle4.html'

    if request.method == 'POST':
        selected_option = request.POST['middle4']
        obj = GameModel.objects.get(id=pk)
        obj.middle4 = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Middle4 player was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)

        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def edit_forward5(request, pk):
    template_name = 'game/edit_forward5.html'

    if request.method == 'POST':
        selected_option = request.POST['forward5']
        obj = GameModel.objects.get(id=pk)
        obj.forward5 = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Forward5 player was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)

        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def edit_forward6(request, pk):
    template_name = 'game/edit_forward6.html'

    if request.method == 'POST':
        selected_option = request.POST['forward6']
        obj = GameModel.objects.get(id=pk)
        obj.forward6 = User.objects.get(id=selected_option)
        obj.save()
        messages.add_message(request, messages.SUCCESS, 'Forward6 player was added.')
        games = GameModel.objects.get(id=pk)
        users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)

        content = {
            'games': games,
            'users': users,
        }
        return render(request, template_name, content)

    games = GameModel.objects.get(id=pk)
    users = User.objects.all().exclude(id=games.goalkeeper_id).exclude(id=games.defense2_id).exclude(id=games.defense3_id).exclude(id=games.middle4_id).exclude(id=games.forward5_id).exclude(id=games.forward6_id)
    content = {
        'games': games,
        'users': users,
    }
    return render(request, template_name, content)

def game_delete(request, pk):
    template_name = 'game/delete_task.html'
    model = GameModel
    game = get_object_or_404(GameModel, id=pk)
    if request.user.is_staff or request.user == game.creator:
        game.delete()
        messages.add_message(request, messages.WARNING, 'Game was deleted.')
        return HttpResponseRedirect(reverse('list_of_my_games'))
    else:
        messages.add_message(request, messages.WARNING, 'You are not creator or staff.')
        return HttpResponseRedirect(reverse('list_of_my_games'))
