from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r"^account/", include("account.urls")),
    url(r'^', include('soccergame.urls')),
    url(r'^logout/$', auth_views.logout, name='logout'),
]
